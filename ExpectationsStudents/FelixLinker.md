# Expectations of Felix Linker
## Where I come from
Having studied computer science for 4 semesters and philosophy for about 3 semesters, digital humanities seems like a welcome addition to my "old" courses. I'm *quite* into software development and the classical topics of computer science. I'm working on my own projects such as [for example a system to generate tweets in RDF](https://github.com/felixlinker/TWIG).

## Where I want to go
I plan on studying master of computer science with focus on big data. Therefore I plan to write my bachelor thesis regarding big data or something similar. I'm very interested in artificial intelligence as it is deeply connected to theoretical computer science.

I'd like to learn new things about:
- Text processing
- Process result storing
- Data access for research

All this is related to statistical learning on big data.
