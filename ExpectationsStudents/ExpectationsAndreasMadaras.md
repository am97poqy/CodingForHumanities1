### Coding for Humanities
## "My Expectations" by Andreas Madaras 

I always wanted to study something related to computer science that focuses on Humanities instead of having a major focus on higher math. 'Major' being the keyword here. I don't hate math. It just is a bit too much in many other bachelor degrees.
Because of this wish i have chosen  to enroll inthis course and this is what I expect from it:

* get to know previously unknown applications for coding
* learn Python and R
* refresh my half forgotten knowledge of xml
* still getting credit for this because I just bought a new laptop to do it

also have a [link](https://git.informatik.uni-leipzig.de/introduction-dh/CodingForHumanities1.git) to your own git
